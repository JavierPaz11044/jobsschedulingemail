#!/usr/bin/python3
import requests

OMD_ID = 848228
URL = "http://www.omdbapi.com/?i=tt0848228&apikey=9698175f"


def responseDataHTML():
    str_data = open('/home/javier/Documentos/University/Algoritmos/JobsScheduling/id').read()
    cont = int(float(str_data))

    print(cont)
    while True:
        str_data = str(cont)
        zeros = 7 - len(str_data)
        id = ''
        for i in range(zeros):
            id += '0'
        id += str_data
        resp = requests.get(f'http://www.omdbapi.com/?i=tt{id}&apikey=9698175f')
        cont += 1
        if resp.json()['Response'] != 'False':
            break
    open('/home/javier/Documentos/University/Algoritmos/JobsScheduling/id', 'w').write(str(cont))

    response_header = '''
    <html lang=\"en\">
<head>
    <meta charset=\"UTF-8\">
    <meta http-equiv=\"X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>TEST</title>
    <style>
        body {
            border: 3px black;
            border-radius: 5% ; 
            display: flex;
            justify-content: center;
            align-items: center;
            
        }
        .container {
            border: 3px wheat solid;
            border-radius: 10% ;
            display: flex;
            justify-content: center;
            align-items: center;
            padding: 3em;
            background-color: gray;
        }

        .container-data{
            display: flex;
            padding: 20px;
            padding-top: 50px;
        }
        .container-data > div {
            width: 400px;
        }
        .container-data > div > div {
            display: flex;
        }
        .container-center {
            display: flex;
            align-items: center;
            justify-content: center;
        }
        .container-description-movie,
        .container-description-movie > div
        {
            border: 3px white solid;
        }
        .container-post {
            margin-right: 10px;
        }
    </style>
</head>
'''
    response_body = f'''
<body>
    <div class="container">
        <div class="container-post">
            <img src=\"{resp.json()['Poster']}\">
        </div>
        <div class="container-description-movie">
            <div class="container-center">
                <h2>
                               {resp.json()['Title']}
                </h2>
            </div>
            <div class="container-data">
                <div class="column-one">
   
                        <div class="container-year">
                            <h3>Year:  </h3>
                        <h4> {resp.json()['Released']}</h4>
                        </div>
                
                        <div class="container-Duration">
                            <h3>Duration:  </h3>
                            <h4> {resp.json()['Runtime']} </h4>
                        </div>
                
                        <div class="container-Genre">
                            <h3>Genre:  </h3>
                            <h4> {resp.json()['Genre']}</h4>
                        </div>
                
                        <div class="container-Director">
                            <h3>Director:  </h3>
                            <h4> {resp.json()['Director']}</h4>
                        </div>
                
                </div>
                <div class="column-two">
                    <div class="container-Writer">
        
                        <h3>Writer:  </h4>
                        <h4> {resp.json()['Writer']}</h4>
                    </div>
            
                    <div class="container-Actors"> 
                        <h3>Actors:  </h3>
                        <h4> {resp.json()['Actors']}</h4>
                    </div>
            
                    <div class="container-Description">
                        <h3>Description:  </h3>
                        <h4> {resp.json()['Plot']}</h4>
                    </div>
                </div>
        
            </div>
        
        </div>
        
    </div>
    </body>
</html>
    '''
    return f'{response_header}\n{response_body}'
